# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'e3b954f6f23da3ee3519dbafda0170ba7f9de32c6ed4d43352be85623741b2bca68eae4d8e2a1047485648cdd5f6c7179f0671bf81e305733bc6475d3c0b9672'
